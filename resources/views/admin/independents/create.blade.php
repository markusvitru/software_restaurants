@extends('layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Crear Empleado Independiente</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form  action="{{ route('independents.store') }}" method="POST" enctype="multipart/from-data">
                @csrf
              <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="phone">Telefóno</label>
                            <input type="text" class="form-control" id="phone" name="phone">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="city">Ciudad</label>
                            <input type="text" class="form-control" id="city" name="city">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="identification">Identificación</label>
                            <input type="text" class="form-control" id="identification" name="identification">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="specialties_id">Especialidad</label>
                            <select name="specialties_id" id="specialties_id" class="form-control" enabled>
                                @foreach($specialties as $specialty)
                                    <option value="{{ $specialty->id }}">{{ $specialty->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="categories_id">Categoria</label>
                            <select name="categories_id" id="categories_id" class="form-control" enabled>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="images">Imagenes Trabajos</label>
                            <div class="input-group">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="images">
                                <label class="custom-file-label" for="images">Cargar logo</label>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-header">
                    <h3 class="card-title">Agregar Horarios de Disponibilidad</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Escoger Días</label>
                                <input name="WorkWeek" class="form-control days-of-week" id="WorkWeek" type="text" value="" data-bind="value: WorkWeek">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="images">Horario Inicio</label>
                                <input type="time" class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="images">Horario Fin</label>
                                <input type="time" class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <button type="button" class="btn btn-block btn-primary">Agregar más horarios de disponibilidad</button>
                        </div>
                    </div>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                  <div class="row">
                    <div class="col-12 text-center">
                        <input type="hidden" class="user_id" id="user_id" name="user_id" value="{{ $user->id }}">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                  </div>
              </div>
            </form>
          </div>
    </div>
    <script>
        (function( $ ){

        "use strict";

        $.fn.daysOfWeekInput = function() {
        return this.each(function(){
            var $field = $(this);

            var days = [
            {
                Name: 'Domingo',
                Value: 'D',
                Checked: false
            },
            {
                Name: 'Lunes',
                Value: 'L',
                Checked: false
            },
            {
                Name: 'Martes',
                Value: 'M',
                Checked: false
            },
            {
                Name: 'Miercoles',
                Value: 'W',
                Checked: false
            },
            {
                Name: 'Jueves',
                Value: 'J',
                Checked: false
            },
            {
                Name: 'Viernes',
                Value: 'V',
                Checked: false
            },
            {
                Name: 'Sábado',
                Value: 'S',
                Checked: false
            }
            ];

            var currentDays = $field.val().split('');
            for(var i = 0; i < currentDays.length; i++) {
            var dayA = currentDays[i];
            for(var n = 0; n < days.length; n++) {
                var dayB = days[n];
                if(dayA === dayB.Value) {
                dayB.Checked = true;
                }
            }
            }

            // Make the field hidden when in production.
            //$field.attr('type','hidden');

            var options = '';
            var n = 0;
            while($('.days' + n).length) {
            n = n + 1;
            }

            var optionsContainer = 'days' + n;
            $field.before('<div class="days ' + optionsContainer + '"></div>');

            for(var i = 0; i < days.length; i++) {
            var day = days[i];
            var id = 'day' + day.Name + n;
            var checked = day.Checked ? 'checked="checked"' : '';
            options = options + '<div><input type="checkbox" value="' + day.Value + '" id="' + id + '" ' + checked + ' /><label for="' + id + '">' + day.Name + '</label>&nbsp;&nbsp;</div>';
            }

            $('.' + optionsContainer).html(options);

            $('body').on('change', '.' + optionsContainer + ' input[type=checkbox]', function () {
            var value = $(this).val();
            var index = getIndex(value);
            if(this.checked) {
                updateField(value, index);
            } else {
                updateField(' ', index);
            }
            });

            function getIndex(value) {
            for(i = 0; i < days.length; i++) {
                if(value === days[i].Value) {
                return i;
                }
            }
            }

            function updateField(value, index) {
            $field.val($field.val().substr(0, index) + value + $field.val().substr(index+1)).change();
            }
        });
        }

        })( jQuery );

        $('.days-of-week').daysOfWeekInput();
    </script>
@endsection
