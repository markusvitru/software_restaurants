<?php
    use Illuminate\Support\Facades\DB;
    use App\Rating;
?>
@extends('layouts.app')
@section('content')
    <div class="content-wrapper">
        <h3 id="result"></h3>
        @if($independent == null)
            <div class="row justify-content-center my-3">
                <a href="{{ url('/admin/independents/create') }}" class="btn btn-primary">Agregar Empleado Independiente</a>
            </div>
        @endif
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="pl-5">Nombre</th>
                    <th class="text-center">Telefono</th>
                    <th class="text-center">Especialidad</th>
                    <th class="text-center">Calificación</th>
                    <th class="col-xs-2 text-right pr-5">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($independents as $c)
                <tr>
                    <td class="pl-5">{{ $c->name }}</td>
                    <td class="text-center">{{ $c->phone }}</td>
                    <td class="text-center">
                        @foreach($specialties as $s)
                            @if($c->specialties_id == $s->id)
                                {{ $s->name }}
                            @endif
                        @endforeach
                    </td>
                    <td class="text-center">
                            <?php $ratings = Rating::select('score','independent_id')->where('independent_id',$c->id)->get(); ?>
                            @if(!$ratings->isEmpty())
                                @foreach($ratings as $r)
                                    @if($c->id == $r->independent_id)
                                        @if($r->score >= 5)
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                        @elseif($r->score >= 4)
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star"></span>
                                        @elseif($r->score >= 3)
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        @elseif($r->score >= 2)
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        @elseif($r->score >= 1)
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        @else
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        @endif
                                    @endif
                                @endforeach
                            @else
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            @endif

                    </td>
                    <td class="text-right pr-5">
                        <?php
                            $consultaq = DB::select(DB::raw("SELECT * FROM company_independents WHERE independent_id='$c->id' AND is_active=1"));
                            if(empty($consultaq)){ ?>
                                <a class="btn_menu btn_delete open-modal-del" href="#" data-id="{{ $c->id }}">
                                    <i class="fa fa-address-card" title="Agregar Empleado Independiente"></i>
                                </a>
                        <?php } ?>

                        @if($user['adm'] != 0)
                            <a href="{{ url('/admin/independents/'.$c->id.'/edit') }}" class="btn_menu">
                                <i class="fa fa-pencil" title="Editar"></i>
                            </a>
                            <a class="btn_menu btn_delete" href="#" data-id="{{ $c->id }}">
                                <i class="fa fa-trash" title="Eliminar"></i>
                            </a>
                        @endif
                        <a class="btn_menu" href="{{ url('/admin/independents/profile/'.$c->id) }}">
                            <i class="fa fa-user" title="Ver Perfil"></i>
                        </a>
                    </td>
                </tr>

                <!-- Modal Add Independient -->
                <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
                id="mi-modal">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                            <div class="content_top">
                                Estos son los horarios de disponibilidad de este empleado por favor seleccione al menos uno antes de agregar el empleado a su plantilla de empleados.
                                <div class="row titles_hours">
                                    <div class="col-sm-1">
                                        <b>Núm.</b>
                                    </div>
                                    <div class="col-sm-6 text-center">
                                        <b>Días</b>
                                    </div>
                                    <div class="text-center col-sm-2">
                                        <b>Inicio</b>
                                    </div>
                                    <div class="text-center col-sm-2">
                                        <b>Fin</b>
                                    </div>
                                    <div class="text-center col-sm-1">
                                    </div>
                                </div>
                                <div class="row" id="hours">

                                </div>
                            </div>
                            <div id="messageHours">
                                <div class='text-center text-danger pt-3'><b>Horario de disponibilidad del empleado agregado correctamente.</b></div>
                            </div>
                            <div class="modal-header content_message pt-0">

                                <span class="modal-title text-center mt-4" id="myModalLabel"><b>Realmente desea agregar este empleado independiente a sus empleados?</b></span>
                                @foreach ($company as $c)
                                    <input type="hidden" value="{{ $c->id }}" name="idcompany" id="idcompany">
                                @endforeach
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
                                <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </tbody>
        </table>
        <input type="hidden" name="idselected" id="idselected">
        <input type="hidden" name="idsHours" id="idsHours">
    </div>
    <script>
        $(document).on("click", ".open-modal-del", function () {
            var dataId_ = $(this).attr("data-id");
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type:'POST',
                url:"{{ route('consultAvailableTime') }}",
                data:{id:dataId_},
                success:function(data){
                    $("#hours").empty();
                    $.each(data,function(key,value){
                        var num = key+1;
                        $("#hours").append('<div class="col-sm-1">'+num+'. '+'</div><div class="col-sm-6 text-center">'+value['days']+'</div><div class="col-sm-2 text-center">'+value['start_time']+'</div></div><div class="col-sm-2 text-center">'+value['end_time']+'</div><div class="col-sm-1 text-center"><a href="#" id="'+value['id']+'" onclick="addHours('+value['id']+","+value['independent_id']+","+num+')"><i class="fa fa-clock-o"></i></a></div>');
                    });
                }
            });
            document.getElementById("idselected").value = dataId_;
        });
        var modalConfirm = function(callback){
          $(".btn_delete").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });

          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
            location.reload();
          });
        };
        modalConfirm(function(confirm){
          if(confirm){
            //Acciones si el usuario confirma
            var _token = $('input[name="_token"]').val();
            var id = $('#idselected').val();
            var ids = $('#idsHours').val();
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type:'POST',
                url:"{{ route('addIndependent') }}",
                data:{id:id,ids:ids},
                success:function(data){
                    $("#result").html("Empleado Independiente Agregado Correctamente");
                }
            });
          }else{
            //Acciones si el usuario no confirma
            $("#result").html("NO CONFIRMADO");
          }
        });
        function addHours(idhour,idindep,num){
            var idh = $('#idsHours').val();
            if(idh != ''){
                idh = idh + '-' + idhour;
                document.getElementById('idsHours').value = idh;
            }else{
                document.getElementById('idsHours').value = idhour;
            }
            $('#messageHours').css('display','block');
            $('#messageHours').append("<div class='text-center'>Número horario: "+num+"</div>");
            document.getElementById(idhour).removeAttribute("onclick");
            $("#"+idhour).html('<i class="fa fa-check"></i>');
        }
    </script>
@endsection
