<?php
    use App\Category;
    use App\Specialty;
    use App\Rating;
?>
@extends('layouts.app')
@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">

                      <!-- Profile Image -->
                      <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                          <div class="text-center">
                              <img src="{{asset('images/profile_independents/blank-profile-picture-973460_640.png')}}" class="profile-user-img img-fluid img-circle" alt="User profile picture">
                          </div>

                          <h3 class="profile-username text-center">{{ $independentinfo['name'] }}</h3>
                          <p class="text-muted text-center">
                              <?php
                                  $category = Category::select('name')->where('id',$independentinfo['categories_id'])->get();
                                  foreach($category as $or){
                                      echo $or->name;
                                  }
                              ?>
                          </p>
                          <p class="text-center">
                            <?php $ratings = Rating::select('score','independent_id')->where('independent_id',$independentinfo->id)->get();?>
                            @if(!$ratings->isEmpty())
                                @foreach($ratings as $r)
                                    @if($independentinfo->id == $r->independent_id)
                                        @if($r->score >= 5)
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                        @elseif($r->score >= 4)
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star"></span>
                                        @elseif($r->score >= 3)
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        @elseif($r->score >= 2)
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        @elseif($r->score >= 1)
                                            <span class="fa fa-star qualification_made"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        @else
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        @endif
                                    @endif
                                @endforeach
                            @else
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            @endif
                          </p>

                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->

                      <!-- About Me Box -->
                      <div class="card card-primary">
                        <div class="card-header">
                          <h3 class="card-title">Acerca de mi</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                          <strong><i class="fas fa-map-marker-alt mr-1"></i> Ciudad</strong>

                          <p class="text-muted">ciudad</p>

                          <hr>

                          <strong><i class="fas fa-pencil-alt mr-1"></i> Especialidades</strong>

                          <p class="text-muted">
                              <?php
                                  $specialty = Specialty::select('name')->where('id',$independentinfo['specialties_id'])->get();
                                  foreach($specialty as $or){?>
                                      <span class="tag tag-danger">{{ $or->name }}</span>
                              <?php }
                              ?>
                          </p>

                          <hr>

                          <strong><i class="far fa-file-alt mr-1"></i> Contacto</strong>

                          <p class="text-muted">Email: <span>{{ $independentinfo['email'] }}</span></p>
                          <p class="text-muted">Teléfono: <span>{{ $independentinfo['phone'] }}</span></p>
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-9">
                      <div class="card">
                        <div class="card-header p-2">
                          <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Trabajos Realizados</a></li>
                            <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Empresas Trabajadas</a></li>
                          </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                          <div class="tab-content">
                            <div class="active tab-pane" id="activity">
                              <!-- Post -->
                              <div class="post">
                                @if($independentinfo['images'] != '')
                                <div class="col-12 col-sm-12">
                                    <div class="row">
                                        <?php
                                            $imgs = explode(' / ', $independentinfo['images']);
                                            $cantidadimgs = count($imgs);
                                            for($i=0;$i<$cantidadimgs;$i++){
                                        ?>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="img-wrap_">
                                                            <img src="{{asset('images/jobs_independents/'.$imgs[$i])}}" class="img-fluid">
                                                            <input type="hidden" name="urlimg<?php echo $i; ?>" id="urlimg<?php echo $i; ?>" value="{{ $imgs[$i] }}">
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php } ?>
                                        <input type="hidden" name="quantity" class="quantity" value="<?php echo $i-1; ?>">
                                    </div>
                                </div>
                            @endif


                              </div>
                              <!-- /.post -->

                              <!-- Post -->

                              <!-- /.post -->
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="timeline">
                              <!-- The timeline -->
                              <div class="timeline timeline-inverse">
                                <!-- timeline time label -->
                                <div class="time-label">
                                  <span class="bg-danger">
                                    10 Feb. 2014
                                  </span>
                                </div>
                                <!-- /.timeline-label -->
                                <!-- timeline item -->
                                <div>
                                  <i class="fas fa-envelope bg-primary"></i>

                                  <div class="timeline-item">
                                    <span class="time"><i class="far fa-clock"></i> 12:05</span>

                                    <h3 class="timeline-header">Empresa Uno</h3>

                                    <div class="timeline-body">
                                      Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                      weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                      jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                      quora plaxo ideeli hulu weebly balihoo...
                                    </div>
                                  </div>
                                </div>
                                <!-- END timeline item -->
                                <!-- timeline item -->
                                <div>
                                  <i class="fas fa-user bg-info"></i>

                                  <div class="timeline-item">
                                    <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>

                                    <h3 class="timeline-header border-0">Empresa Dos
                                    </h3>
                                    <div class="timeline-body">
                                      Take me to your leader!
                                      Switzerland is small and neutral!
                                      We are more like Germany, ambitious and misunderstood!
                                    </div>
                                  </div>
                                </div>
                                <!-- END timeline item -->

                                <!-- /.timeline-label -->
                                <!-- timeline item -->
                                <div>
                                  <i class="fas fa-camera bg-purple"></i>

                                  <div class="timeline-item">
                                    <span class="time"><i class="far fa-clock"></i> 2 days ago</span>

                                    <h3 class="timeline-header">Empresa Tres</h3>

                                    <div class="timeline-body">
                                      <img src="http://placehold.it/150x100" alt="...">
                                      <img src="http://placehold.it/150x100" alt="...">
                                      <img src="http://placehold.it/150x100" alt="...">
                                      <img src="http://placehold.it/150x100" alt="...">
                                    </div>
                                  </div>
                                </div>
                                <!-- END timeline item -->
                                <div>
                                  <i class="far fa-clock bg-gray"></i>
                                </div>
                              </div>
                            </div>
                            <!-- /.tab-pane -->


                          </div>
                          <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                      </div>
                      <!-- /.nav-tabs-custom -->
                    </div>
                    <!-- /.col -->
                  </div>
              <!-- /.row -->
            </div><!-- /.container-fluid -->
          </section>
    </div>
@endsection
