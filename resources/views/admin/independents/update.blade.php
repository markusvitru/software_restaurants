@extends('layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Editar Empleado Independiente</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('independents.update',$indep->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
              <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$indep->name}}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="phone">Telefóno</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{$indep->phone}}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="city">Ciudad</label>
                            <input type="text" class="form-control" id="city" name="city" value="{{$indep->city}}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{$indep->email}}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="identification">Identificación</label>
                            <input type="text" class="form-control" id="identification" name="identification" value="{{$indep->identification}}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="specialties_id">Especialidad</label>
                            <select name="specialties_id" id="specialties_id" class="form-control" enabled>
                                @foreach($specialties as $specialty)
                                    <option value="{{ $specialty->id }}" <?php if($specialty->id==$indep->specialties_id){ echo "selected"; } ?>>{{ $specialty->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="categories_id">Categoria</label>
                            <select name="categories_id" id="categories_id" class="form-control" enabled>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" <?php if($category->id==$indep->categories_id){ echo "selected"; } ?>>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="images">Imagenes Trabajos</label>
                            <div class="input-group">
                              <div class="custom-file">
                                <input type="file" name="files[]" multiple>
                              </div>
                            </div>
                        </div>
                    </div>
                    @if($indep->images != '')
                        <div class="col-12 col-sm-12">
                            <div class="row">
                                <?php
                                    $imgs = explode(' / ', $indep->images);
                                    $cantidadimgs = count($imgs);
                                    for($i=0;$i<$cantidadimgs;$i++){
                                ?>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="img-wrap_">
                                                    <span onClick="sendNumber(<?php echo $i; ?>);" class="close_" id="close_">&times;</span>
                                                    <img src="{{asset('images/jobs_independents/'.$imgs[$i])}}" class="img-fluid">
                                                    <input type="hidden" name="urlimg<?php echo $i; ?>" id="urlimg<?php echo $i; ?>" value="{{ $imgs[$i] }}">
                                                </div>
                                            </div>
                                        </div>
                                <?php } ?>
                                <input type="hidden" name="quantity" class="quantity" value="<?php echo $i-1; ?>">
                            </div>
                        </div>
                    @endif
                </div>
              </div>
              <!-- /.card-body -->
              @if(!$availableTime->isEmpty())
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Horarios Disponibles Habilitados</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="pl-5">Días</th>
                                    <th class="text-center">Hora Inicio</th>
                                    <th class="text-center">Hora Fin</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($availableTime as $av)
                                    <tr>
                                        <td>{{ $av->days }}</td>
                                        <td class="text-center">{{ $av->start_time }}</td>
                                        <td class="text-center">{{ $av->end_time }}</td>
                                        <td class="text-center">
                                            <a class="btn_menu btn_delete open-modal-del" href="#" data-id="{{ $av->id }}">
                                                <i class="fa fa-trash" title="Eliminar"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <input type="hidden" name="idselected" id="idselected">
                    </div>
                </div>
              @endif
              <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Agregar Horarios de Disponibilidad</h3>
                </div>
              </div>
            <div class="card-body">
                <div class="row duplicate">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Escoger Días</label>
                            <input name="days1" class="form-control days-of-week" id="days1" type="text" value="" data-bind="value: WorkWeek">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label for="images">Horario Inicio</label>
                            <input type="time" class="form-control" name="start_time1" id="start_time1">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label for="images">Horario Fin</label>
                            <input type="time" class="form-control" name="end_time1" id="end_time1">
                        </div>
                    </div>
                </div>
                <div class="show_item" id="dinamicos1">
                </div>
                <div class="row">
                    <div class="col-12">
                        <button type="button" class="btn btn-block btn-primary" onclick="detectarEstado();">Agregar más horarios de disponibilidad</button>
                    </div>
                </div>
            </div>
          </div>
          <!-- /.card-body -->

            <div class="card-footer">
                <div class="row">
                <div class="col-12 text-center">
                    <input type="hidden" name="numberimg" id="numberimg">
                    <input type="hidden" name="id_a" class="id_a" value="{{ $indep->id }}">
                    <input type="hidden" name="images" value="{{ $indep->images }}">
                    <input type="hidden" class="user_id" id="user_id" name="user_id" value="{{ $user->id }}">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                </div>
            </div>
        </form>
        </div>
    </div>
    <!-- Modal eliminar horarios -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
                id="mi-modal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header content_message">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <span class="modal-title text-center mt-4" id="myModalLabel"><b>Realmente desea eliminar este horario?</b></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
                    <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal eliminar imágenes -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modalimg">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Realmente desea eliminar la imagen?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" id="modal-btn-si_">Si</button>
              <button type="button" class="btn btn-primary" id="modal-btn-no_">No</button>
            </div>
          </div>
        </div>
      </div>
    <script>
        function sendNumber(number){
            document.getElementById("numberimg").value = number;
        }
        $(document).on("click", ".close_", function () {
            var dataId_ = $(this).attr("data-id");
            document.getElementById("idselected").value = dataId_;


            var modalConfirm = function(callback){
              $(".img-wrap_ .close_").on("click", function(){
                $("#mi-modalimg").modal('show');
              });
              $("#modal-btn-si_").on("click", function(){
                callback(true);
                $("#mi-modalimg").modal('hide');
              });
              $("#modal-btn-no_").on("click", function(){
                callback(false);
                $("#mi-modalimg").modal('hide');
              });
            };
            modalConfirm(function(confirm){
              if(confirm){
                //Acciones si el usuario confirma
                var query = $('.images').val();
                if(query != ''){
                    var _token = $('input[name="_token"]').val();
                    var ida = $('.id_a').val();
                    var quantity = $('.quantity').val();
                    var number = $('#numberimg').val();
                    var url = 'urlimg'+number;
                    var delimg = 0;
                    var counter = 1;
                    var uimgs = '';
                    for(var i=0;i<=quantity;i++){
                        if(i==number){
                            delimg = document.getElementById(url).value;
                        }else{
                            var imgs = 'urlimg'+i;
                            var u = 'urlimg'+i;
                            if(counter==1){
                                u = document.getElementById(imgs).value;
                                uimgs = u;
                            }else{
                                u = document.getElementById(imgs).value;
                                uimgs = uimgs+" / "+u;
                            }
                            counter++;
                        }
                    }
                    $.ajax({
                        url:"{{ route('deleteImg') }}",
                        method:"POST",
                        data:{uimgs:uimgs,ida:ida,delimg:delimg,_token:_token},
                        success:function(data){
                            location.reload();
                        }
                    });
                }
              }else{
                //Acciones si el usuario no confirma
                $("#result").html("NO CONFIRMADO");
              }
            });
        });
        var modalConfirm = function(callback){
          $(".btn_delete").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });

          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
        };
        modalConfirm(function(confirm){
          if(confirm){
            //Acciones si el usuario confirma
            var _token = $('input[name="_token"]').val();
            var id = $('#idselected').val();
            console.log(id);
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type:'POST',
                url:"{{ route('deleteAvailableTime') }}",
                data:{id:id},
                success:function(data){
                    location.reload();
                    alert("Horario Eliminado Correctamente.");
                }
            });
          }else{
            //Acciones si el usuario no confirma
            $("#result").html("NO CONFIRMADO");
          }
        });
        function detectarEstado(){
            var cantidad = $('.duplicate').length + 1;
            var c = $('.duplicate').length;
            $('#dinamicos'+c).after(
                '<div id="dinamicos'+cantidad+'"><h3>Horario Número '+cantidad+'</h3><div class="row duplicate"><div class="col-12"><div class="form-group"><label>Escribir Días</label><input name="days'+cantidad+'" class="form-control days-of-week" id="days'+cantidad+'" type="text" value=""></div></div><div class="col-12 col-sm-6"><div class="form-group"><label for="images">Horario Inicio</label><input type="time" name="start_time'+cantidad+'" id="start_time'+cantidad+'" class="form-control"></div></div><div class="col-12 col-sm-6"><div class="form-group"><label for="images">Horario Fin</label><input type="time" class="form-control" name="end_time'+cantidad+'" id="end_time'+cantidad+'"></div></div></div>'
            );
        }
        (function( $ ){

            "use strict";

            $.fn.daysOfWeekInput = function() {
            return this.each(function(){
                var $field = $(this);

                var days = [
                {
                    Name: 'Domingo',
                    Value: 'D',
                    Checked: false
                },
                {
                    Name: 'Lunes',
                    Value: 'L',
                    Checked: false
                },
                {
                    Name: 'Martes',
                    Value: 'M',
                    Checked: false
                },
                {
                    Name: 'Miercoles',
                    Value: 'W',
                    Checked: false
                },
                {
                    Name: 'Jueves',
                    Value: 'J',
                    Checked: false
                },
                {
                    Name: 'Viernes',
                    Value: 'V',
                    Checked: false
                },
                {
                    Name: 'Sábado',
                    Value: 'S',
                    Checked: false
                }
                ];

                var currentDays = $field.val().split('');
                for(var i = 0; i < currentDays.length; i++) {
                var dayA = currentDays[i];
                for(var n = 0; n < days.length; n++) {
                    var dayB = days[n];
                    if(dayA === dayB.Value) {
                    dayB.Checked = true;
                    }
                }
                }

                // Make the field hidden when in production.
                //$field.attr('type','hidden');

                var options = '';
                var n = 0;
                while($('.days' + n).length) {
                n = n + 1;
                }

                var optionsContainer = 'days' + n;
                $field.before('<div class="days ' + optionsContainer + '"></div>');

                for(var i = 0; i < days.length; i++) {
                var day = days[i];
                var id = 'day' + day.Name + n;
                var checked = day.Checked ? 'checked="checked"' : '';
                options = options + '<div><input type="checkbox" value="' + day.Value + '" id="' + id + '" ' + checked + ' /><label for="' + id + '">' + day.Name + '</label>&nbsp;&nbsp;</div>';
                }

                $('.' + optionsContainer).html(options);

                $('body').on('change', '.' + optionsContainer + ' input[type=checkbox]', function () {
                var value = $(this).val();
                var index = getIndex(value);
                if(this.checked) {
                    updateField(value, index);
                } else {
                    updateField(' ', index);
                }
                });

                function getIndex(value) {
                for(i = 0; i < days.length; i++) {
                    if(value === days[i].Value) {
                    return i;
                    }
                }
                }

                function updateField(value, index) {
                $field.val($field.val().substr(0, index) + value + $field.val().substr(index+1)).change();
                }
            });
            }

            })( jQuery );

            $('.days-of-week').daysOfWeekInput();
    </script>
@endsection
