<?php
    use App\CompanyIndependent;
    use App\Company;
?>
@extends('layouts.app')
@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            @if(!$availableTime->isEmpty())
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Estado Horarios Disponibilidad</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="pl-5">Días</th>
                                    <th class="text-center">Hora Inicio</th>
                                    <th class="text-center">Hora Fin</th>
                                    <th class="text-center">Empresa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($availableTime as $av)
                                    <tr>
                                        <td>{{ $av->days }}</td>
                                        <td class="text-center">{{ $av->start_time }}</td>
                                        <td class="text-center">{{ $av->end_time }}</td>
                                        <td class="text-center">
                                            <?php $companyIndependent = CompanyIndependent::select('available_time_id','company_id')->where('independent_id',$independentinfo['id'])->get(); ?>
                                            @if(!$companyIndependent->isEmpty())
                                                <?php $arr; ?>
                                                @foreach($companyIndependent as $ci)
                                                    <?php
                                                        $arr = explode("-",$ci['available_time_id']);
                                                        $size = count($arr);
                                                        for($i = 0;$i < $size;$i++){
                                                            if($arr[$i] == $av->id){
                                                                $ncompany = Company::select('name')->where('id',$ci['company_id'])->first();
                                                                echo $ncompany['name'];
                                                            }
                                                        }
                                                    ?>
                                                @endforeach
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <input type="hidden" name="idselected" id="idselected">
                    </div>
                </div>
              @else
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Estado Horarios Disponibilidad</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="pl-5">Días</th>
                                    <th class="text-center">Hora Inicio</th>
                                    <th class="text-center">Hora Fin</th>
                                    <th class="text-center">Empresa</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
              @endif
        </section>
    </div>
@endsection
