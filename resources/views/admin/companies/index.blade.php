<?php
?>
@extends('layouts.app')
@section('content')
    <div class="content-wrapper">
        @if($company->count() > 0)
        @else
            <div class="row justify-content-center my-3">
                <a href="{{ url('/admin/companies/create') }}" class="btn btn-primary">Agregar Empresa</a>
            </div>
        @endif
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="pl-5">Nombre</th>
                    <th>Telefono</th>
                    <th>Especialidad</th>
                    <th class="col-xs-2 text-right pr-5">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($companies as $c)
                <tr>
                    <td class="pl-5">{{ $c->name }}</td>
                    <td>{{ $c->phone }}</td>
                    <td>
                        @foreach($specialties as $s)
                            @if($c->specialties_id == $s->id)
                                {{ $s->name }}
                            @endif
                        @endforeach
                    </td>
                    <td class="text-right pr-5" >
                        <a href="{{ url('/admin/companies/'.$c->id.'/edit') }}" class="btn_menu">
                            <i class="fa fa-pencil" title="Editar"></i>
                        </a>
                        <a class="btn_menu btn_delete open-modal-del" href="#" data-id="{{ $c->id }}">
                            <i class="fa fa-trash" title="Eliminar"></i>
                        </a>
                    </td>
                </tr>

                @endforeach
            </tbody>
        </table>
        <input type="hidden" name="idselected" id="idselected">
    </div>
@endsection
