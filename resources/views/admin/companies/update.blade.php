@extends('layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Editar Empresa</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form  action="{{ route('companies.update',$comp->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
              <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $comp->name }}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="phone">Telefóno</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{ $comp->phone }}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="city">Ciudad</label>
                            <input type="text" class="form-control" id="city" name="city" value="{{ $comp->city }}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $comp->email }}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="address">Dirección</label>
                            <input type="text" class="form-control" id="address" name="address" value="{{ $comp->address }}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="web_site">Sitio Web</label>
                            <input type="text" class="form-control" id="web_site" name="web_site" value="{{ $comp->web_site }}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="facebook">Facebook</label>
                            <input type="text" class="form-control" id="facebook" name="facebook" value="{{ $comp->facebook }}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="instagram">Instagram</label>
                            <input type="text" class="form-control" id="instagram" name="instagram" value="{{ $comp->instagram }}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="contact">Persona Contacto</label>
                            <input type="text" class="form-control" id="contact" name="contact" value="{{ $comp->contact }}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="specialties_id">Especialidad</label>
                            <select name="specialties_id" id="specialties_id" class="form-control" enabled>
                                @foreach($specialties as $specialty)
                                    <option value="{{ $specialty->id }}" <?php if($specialty->id==$comp->specialties_id){ echo "selected"; } ?>>{{ $specialty->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="categories_id">Categoria</label>
                            <select name="categories_id" id="categories_id" class="form-control" enabled>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" <?php if($category->id==$comp->categories_id){ echo "selected"; } ?>>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="exampleInputFile">Logo</label>
                            <div class="input-group">
                                <?php
                                    if($comp->images != 'No Fotos'){
                                ?>
                                        <div class="img-wrap">
                                            <span class="close">&times;</span>
                                            <img src="{{asset('images/logos_companies/'.$comp->images)}}" class="img-fluid">
                                            <input type="hidden" name="url_logo" class="url_logo" value="{{ $comp->images }}">
                                            <input type="hidden" name="id_hotel" class="id_hotel" value="{{ $comp->id }}">
                                        </div>
                                <?php }else{ ?>
                                        <div class="custom-file">
                                            <input id="logo" class="form-control" name="logo" type="file">
                                        </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                  <div class="row">
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                  </div>
              </div>
            </form>
          </div>
    </div>
@endsection
