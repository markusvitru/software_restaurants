@extends('layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="row justify-content-center my-3">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modalForm">Agregar Categoria</button>
        </div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="pl-5 font-weight-bold">Nombre</th>
                    <th class="pl-5">Tipo</th>
                    <th class="col-xs-2 text-right pr-5 font-weight-bold">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($category as $c)
                <tr>
                    <td class="pl-5">{{ $c->name }}</td>
                    <td class="pl-5">
                        @if($c->type=='empr')
                            Empresa
                        @elseif($c->type=='empl')
                            Empleado Independiente
                        @endif
                    </td>
                    <td class="text-right pr-5">
                        <a href="" class="btn_menu" data-toggle="modal" data-target="#editCatergory{{$c->id}}">
                            <i class="fa fa-pencil" title="Editar"></i>
                        </a>
                        <a class="btn_menu btn_delete open-modal-del" href="#" data-id="{{ $c->id }}">
                            <i class="fa fa-trash" title="Eliminar"></i>
                        </a>
                    </td>
                </tr>

                <div class="modal fade" id="editCatergory{{$c->id}}" role="dialog"
                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('categories.update', $c->id) }}" enctype="multipart/from-data" method="POST">
                                @method('PUT')
                                @csrf

                                <input type="hidden" name="id" value="{{$c->id}}">
                                <!-- Encabezado modal -->
                                <div class="modal-header">
                                    <button type="button" class="close m-0" data-dismiss="modal"
                                        aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"> Actualizar Categoria </h4>
                                </div>

                                <!-- Cuerpo modal -->
                                <div class="modal-body">
                                    <p class="statusMsg"> </p>
                                    <div class="form-group">
                                        <label for="inputName"> Nombre </label>
                                        <input type="text" value="{{$c->name}}" class="form-control" name="name"
                                            placeholder="Ingrese su nombre" />
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="inputName"> Tipo </label>
                                        <select class="form-control" name="typeUpdate" id="typeUpdate">
                                            <option value="0" <?php if($c->type=='0'){ echo "selected"; } ?>>Seleccione el Tipo</option>
                                            <option value="empr" <?php if($c->type=='empr'){ echo "selected"; } ?>>Empresa</option>
                                            <option value="empl" <?php if($c->type=='empl'){ echo "selected"; } ?>>Empleado Independiente</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Pie de página modal -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary submitBtn">
                                        Actualizar </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
            </tbody>
        </table>
        <input type="hidden" name="idselected" id="idselected">
    </div>
    <!-- Modal Save Categoria -->
    <div class="modal fade" id="modalForm" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Encabezado modal -->
                <div class="modal-header">
                    <button type="button" class="close m-0" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"> Agregar Categoria </h4>
                </div>
                <!-- Cuerpo modal -->
                <div class="modal-body">
                    <p class="statusMsg"> </p>
                    <form role="form">
                        <div class="form-group">
                            <label for="inputName"> Nombre </label>
                            <input type="text" class="form-control" id="inputName" placeholder="Ingrese su nombre" />
                        </div>
                        <div class="form-group">
                            <label for="inputName"> Tipo </label>
                            <select class="form-control" name="type" id="type">
                                <option value="0">Seleccione el Tipo</option>
                                <option value="empr">Empresa</option>
                                <option value="empl">Empleado Independiente</option>
                              </select>
                        </div>
                    </form>
                </div>
                <!-- Pie de página modal -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary submitBtn" onclick="submitContactForm()"> Guardar </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Delete -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
    id="mi-modal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header content_message">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <span class="modal-title text-center mt-4" id="myModalLabel"><b>Realmente desea eliminar la categoria?</b></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
                    <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).on("click", ".open-modal-del", function () {
            var dataId_ = $(this).attr("data-id");
            document.getElementById("idselected").value = dataId_;
        });
        var modalConfirm = function(callback){
          $(".btn_delete").on("click", function(){
            $("#mi-modal").modal('show');
          });

          $("#modal-btn-si").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });

          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
        };
        modalConfirm(function(confirm){
          if(confirm){
            //Acciones si el usuario confirma
            var _token = $('input[name="_token"]').val();
            var id = $('#idselected').val();
            $.ajax({
                url:"{{ route('deleteCategorie') }}",
                method:"POST",
                data:{_token:_token,id:id},
                success:function(data){
                    location.reload();
                }
            });
          }else{
            //Acciones si el usuario no confirma
            $("#result").html("NO CONFIRMADO");
          }
        });
        function submitContactForm(){
            var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
            var name = $('#inputName').val();
            console.log(name+'NAME');
            if(name.trim() == '' ){
                alert('Por favor ingrese el nombre de la categoría.');
                $('#inputName').focus();
                return false;
            }else{
                var e = document.getElementById("type");
                var type = e.options[e.selectedIndex].value;
                console.log(type);
                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type:'POST',
                    url:'{{ route("createcategoria") }}',
                    data:'contactFrmSubmit=1&name='+name+'&type='+type,
                    beforeSend: function () {
                        $('.submitBtn').attr("disabled","disabled");
                        $('.modal-body').css('opacity', '.5');
                    },
                    success:function(msg){
                        location.reload();
                    }
                });
            }
        }
    </script>
@endsection
