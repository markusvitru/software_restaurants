<?php
    use App\Category;
    use App\Specialty;
?>
@extends('layouts.public')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Registro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Escoger tipo de registro</label>

                            <div class="col-md-6">
                                <select class="form-control" name="typeform" id="typeform" onchange="ValidarForm()">
                                    <option value="0">Seleccione el Tipo</option>
                                    <option value="empr">Empresa</option>
                                    <option value="empl">Empleado Independiente</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre Completo') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div id="company">
                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">Telefóno</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="phone_company" name="phone_company">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">Ciudad</label>
                                <div class="col-12 col-md-6">
                                    <input type="text" class="form-control" id="city_company" name="city_company">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">Dirección</label>
                                <div class="col-12 col-md-6">
                                    <input type="text" class="form-control" id="address" name="address">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="web_site" class="col-md-4 col-form-label text-md-right">Sitio Web</label>
                                <div class="col-12 col-md-6">
                                    <input type="text" class="form-control" id="web_site" name="web_site">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="facebook" class="col-md-4 col-form-label text-md-right">Facebook</label>
                                <div class="col-12 col-md-6">
                                    <input type="text" class="form-control" id="facebook" name="facebook">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="instagram" class="col-md-4 col-form-label text-md-right">Instagram</label>
                                <div class="col-12 col-md-6">
                                    <input type="text" class="form-control" id="instagram" name="instagram">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="contact" class="col-md-4 col-form-label text-md-right">Persona Contacto</label>
                                <div class="col-12 col-md-6">
                                    <input type="text" class="form-control" id="contact" name="contact">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="specialties_id" class="col-md-4 col-form-label text-md-right">Especialidad</label>
                                <div class="col-12 col-md-6">
                                    <select name="specialties_id_company" id="specialties_id_company" class="form-control" enabled>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="categories_id" class="col-md-4 col-form-label text-md-right">Categoria</label>
                                <div class="col-12 col-md-6">
                                    <select name="categories_id_company" id="categories_id_company" class="form-control" enabled>
                                    </select>
                                </div>
                            </div>
                            <!---->
                        </div>
                        <!-- Indepent -->
                        <div id="indepent">
                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">Telefóno</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="phone" name="phone">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">Ciudad</label>
                                <div class="col-12 col-md-6">
                                    <input type="text" class="form-control" id="city" name="city">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="identification" class="col-md-4 col-form-label text-md-right">Identificación</label>
                                <div class="col-12 col-md-6">
                                    <input type="text" class="form-control" id="identification" name="identification">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="specialties_id" class="col-md-4 col-form-label text-md-right">Especialidad</label>
                                <div class="col-12 col-md-6">
                                    <select name="specialties_id" id="specialties_id" class="form-control" enabled>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="categories_id" class="col-md-4 col-form-label text-md-right">Categoria</label>
                                <div class="col-12 col-md-6">
                                    <select name="categories_id" id="categories_id" class="form-control" enabled>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Tipos de Usuarios</h5>
          <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>-->
        </div>
        <div class="modal-body">
            <div class="form-group row">
                <label for="name" class="col-md-6 col-form-label text-md-right">Escoger tipo de registro</label>

                <div class="col-md-6">
                    <select class="form-control" name="type" id="type">
                        <option value="ninguno">Seleccione el Tipo</option>
                        <option value="empr">Empresa</option>
                        <option value="empl">Empleado Independiente</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="Validar()">Validar</button>
        </div>
      </div>
    </div>
  </div>
<script>
    $(window).on('load', function() {
        $('#myModal').modal('show');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    function Validar(){
        var e = document.getElementById("type");
        var type = e.options[e.selectedIndex].value;
        if(type == 'empr'){
            $('#myModal').modal('hide');
            $('#company').css('display','block');
            $('#indepent').css('display','none');
            document.getElementById("typeform").value = 'empr';
            $.ajax({
                url:"{{ route('getType') }}",
                method:"POST",
                data:{type:type},
                success:function(res){
                    if(res == ''){
                        console.log('Sin data');
                    }else{
                        $.each(res,function(key,value){
                            if(key == "specialtiesempr"){
                                console.log('Valor s-> ' + s);
                                console.log('Especialidad Empresa Key-> ' + key);
                                console.log(value);
                                $.each(value,function(key,v){
                                    $("#specialties_id_company").append('<option value="'+v['id']+'">'+v['name']+'</option>');
                                    s++;
                                });
                            }
                            if(key == "categoriesempr"){
                                $.each(value,function(key,v){
                                    $("#categories_id_company").append('<option value="'+v['id']+'">'+v['name']+'</option>');
                                    c++;
                                });
                            }
                        });

                    }
                }
            });
        }else if(type == 'empl'){
            var s = 0;
            var c = 0;
            $('#myModal').modal('hide');
            $('#indepent').css('display','block');
            $('#company').css('display','none');
            document.getElementById("typeform").value = 'empl';
            $.ajax({
                url:"{{ route('getType') }}",
                method:"POST",
                data:{type:type},
                success:function(res){
                    if(res == ''){
                        console.log('Sin data');
                    }else{
                        $.each(res,function(key,value){
                            if(key == "specialties"){
                                console.log('Valor s-> ' + s);
                                console.log('Especialidad Empleado Key-> ' + key);
                                console.log(value);
                                $.each(value,function(key,v){
                                    $("#specialties_id").append('<option value="'+v['id']+'">'+v['name']+'</option>');
                                    s++;
                                });
                            }
                            if(key == "categories"){
                                $.each(value,function(key,v){
                                    $("#categories_id").append('<option value="'+v['id']+'">'+v['name']+'</option>');
                                    c++;
                                });
                            }
                        });
                    }
                }
            });
        }
    }

    function ValidarForm(){
        var eform = document.getElementById("typeform");
        var typeform = eform.options[eform.selectedIndex].value;
        if(typeform == 'empr'){
            $('#company').css('display','block');
            $('#indepent').css('display','none');
            $.ajax({
                url:"{{ route('getType') }}",
                method:"POST",
                data:{type:type},
                success:function(res){
                    if(res == ''){
                        console.log('Sin data');
                    }else{
                        $.each(res,function(key,value){
                            if(key == "specialtiesempr"){
                                console.log('Valor s-> ' + s);
                                console.log('Especialidad Empresa Key-> ' + key);
                                console.log(value);
                                $.each(value,function(key,v){
                                    $("#specialties_id_company").append('<option value="'+v['id']+'">'+v['name']+'</option>');
                                    s++;
                                });
                            }
                            if(key == "categoriesempr"){
                                $.each(value,function(key,v){
                                    $("#categories_id_company").append('<option value="'+v['id']+'">'+v['name']+'</option>');
                                    c++;
                                });
                            }
                        });

                    }
                }
            });
        }else if(typeform == 'empl'){
            var s = 0;
            var c = 0;
            $('#indepent').css('display','block');
            $('#company').css('display','none');
            $.ajax({
                url:"{{ route('getType') }}",
                method:"POST",
                data:{type:typeform},
                success:function(resp){
                    if(resp == ''){
                        console.log('Sin data');
                    }else{
                        $.each(resp,function(key,value){
                            if(key == "specialties"){
                                console.log('Valor s-> ' + s);
                                console.log('Especialidad Empleado Key-> ' + key);
                                console.log(value);
                                $.each(value,function(key,v){
                                    $("#specialties_id").append('<option value="'+v['id']+'">'+v['name']+'</option>');
                                    s++;
                                });
                            }
                            if(key == "categories"){
                                $.each(value,function(key,v){
                                    $("#categories_id").append('<option value="'+v['id']+'">'+v['name']+'</option>');
                                    c++;
                                });
                            }
                        });
                    }
                }
            });
        }
    }
</script>
@endsection
