
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Plataforma</title>
  <link rel="stylesheet" href="/css/app.css">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <div>
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
    </div>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item d-none d-sm-inline-block">
          <a class="nav-link">
            {{ Auth::user()->name }}
          </a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('logout') }}"
        onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();" class="nav-link">Cerrar Sesión</a>
      </li>
    </ul>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
</div>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link">
      Logo
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block">Modulos</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            @if($company != null)
                @if($company->count() > 0)
                    @foreach($company as $c)
                        <li class="nav-item has-treeview">
                            <a href="{{ url('/admin/companies/'.$c['id'].'/edit') }}" class="nav-link">
                                <i class="fa fa-pencil-square-o"></i>
                                <p>
                                    Editar Datos
                                    <i class="fa fa-chevron-right"></i>
                                </p>
                            </a>
                        </li>
                    @endforeach
                    <li class="nav-item has-treeview">
                        <a href="{{ url('/admin/independents') }}" class="nav-link">
                            <i class="fa fa-users"></i>
                            <p>
                                Trabajadores Independientes
                                <i class="fa fa-chevron-right"></i>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/admin/ratings') }}" class="nav-link">
                            <i class="fa fa-check-square-o"></i>
                            <p>
                                Independientes Activos
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="fa fa-cog"></i>
                            <p>
                                Vacantes
                                <span class="right badge badge-danger">Proximamente</span>
                                <i class="fa fa-chevron-right"></i>
                            </p>
                        </a>
                    </li>
                @endif
            @endif
            @if($independent != [])
                @if($independent->count() > 0)
                    @foreach($independent as $i)
                        <li class="nav-item has-treeview">
                            <a href="{{ url('home/') }}" class="nav-link">
                                <i class="fa fa-user"></i>
                                <p>
                                    Perfil
                                    <i class="fa fa-chevron-right"></i>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="{{ url('/admin/independents/'.$i['id'].'/edit') }}" class="nav-link">
                                <i class="fa fa-pencil-square-o"></i>
                                <p>
                                    Editar Datos
                                    <i class="fa fa-chevron-right"></i>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="{{ url('/admin/independents/occupation/'.$i['id']) }}" class="nav-link">
                                <i class="fa fa-clock-o"></i>
                                <p>
                                    Ocupación Horarios
                                    <i class="fa fa-chevron-right"></i>
                                </p>
                            </a>
                        </li>
                    @endforeach
                @endif
            @endif
            @if($user['adm'] == 1)
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                    <i class="fa fa-cog"></i>
                    <p>
                        Empresas
                        <i class="fa fa-chevron-right"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('/admin/companies') }}" class="nav-link">
                                <i class="fa fa-circle"></i>
                                <p>Consultar</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/companies/create') }}" class="nav-link">
                                <i class="fa fa-circle"></i>
                                <p>Registrar</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/admin/categories') }}" class="nav-link">
                      <i class="fa fa-th"></i>
                      <p>
                        Categorias
                      </p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="{{ url('/admin/specialties') }}" class="nav-link">
                      <i class="fa fa-th"></i>
                      <p>
                        Especialidades
                      </p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="fa fa-th"></i>
                      <p>
                        Membresias
                      </p>
                    </a>
                  </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                    <i class="fa fa-cog"></i>
                    <p>
                        Trabajadores Independientes
                        <i class="fa fa-chevron-right"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                        <a href="{{ url('/admin/independents') }}" class="nav-link">
                            <i class="fa fa-circle"></i>
                            <p>Consultar</p>
                        </a>
                        </li>

                        <li class="nav-item">
                        <a href="{{ url('/admin/independents/create') }}" class="nav-link">
                            <i class="fa fa-circle"></i>
                            <p>Registrar</p>
                        </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="fa fa-th"></i>
                    <p>
                        Calificaciones
                    </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="fa fa-th"></i>
                    <p>
                        Registrar Ventas
                    </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="fa fa-th"></i>
                    <p>
                        Inventario
                    </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                    <i class="fa fa-cog"></i>
                    <p>
                        Vacantes
                        <span class="right badge badge-danger">Proximamente</span>
                        <i class="fa fa-chevron-right"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                        <i class="fa fa-circle"></i>
                        <p>Registrar</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                        <i class="fa fa-circle"></i>
                        <p>Consultar</p>
                        </a>
                    </li>
                    </ul>
                </li>
            @endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content_center">
    @yield('content')
  </div>

  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#    ">Plataform</a>.</strong> Todos los derechos reservados.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="/js/app.js"></script>
</body>
</html>

