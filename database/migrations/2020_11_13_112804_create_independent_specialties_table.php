<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndependentSpecialtiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('independent_specialties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('specialty_id')->unsigned()->index();
            $table->foreign('specialty_id')->references('id')->on('specialties');
            $table->integer('independent_id')->unsigned()->index();
            $table->foreign('independent_id')->references('id')->on('independents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('independent_specialties');
    }
}
