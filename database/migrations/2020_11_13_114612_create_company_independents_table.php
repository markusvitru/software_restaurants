<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyIndependentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_independents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('independent_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->string('available_time_id');
            $table->tinyInteger('is_active')->unsigned()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_independents');
    }
}
