<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndependentAvailableTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('independent_available_times', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('available_time_id')->unsigned()->index();
            $table->foreign('available_time_id')->references('id')->on('available_times');
            $table->integer('independent_id')->unsigned()->index();
            $table->foreign('independent_id')->references('id')->on('independents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('independent_available_times');
    }
}
