<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name')->unique();
            $table->string('city');
            $table->string('phone');
            $table->string('email');
            $table->string('web_site')->nullable();
            $table->unsignedInteger('specialties_id')->unsigned();
            $table->foreign('specialties_id')->references('id')->on('specialties');
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('images')->nullable()->default('No Fotos');
            $table->string('contact')->nullable();
            $table->string('address');
            $table->unsignedInteger('categories_id')->unsigned();
            $table->foreign('categories_id')->references('id')->on('categories');
            $table->tinyInteger('is_active')->unsigned()->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
