<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/auth/register/getType', 'App\Http\Controllers\Auth\RegisterController@getType')->name('getType');
Route::post('/admin/categories/getCategory', 'App\Http\Controllers\Admin\CategoriesController@getCategory')->name('getCategory');
Auth::routes();

Route::middleware('auth')->get('/home', 'App\Http\Controllers\HomeController@index')->name('home');
Route::middleware('auth')->post('/admin/categories/createcategoria', 'App\Http\Controllers\Admin\CategoriesController@createcategoria')->name('createcategoria');
Route::middleware('auth')->post('/admin/categorie/deleteCategorie', 'App\Http\Controllers\Admin\CategoriesController@deleteCategorie')->name('deleteCategorie');
Route::middleware('auth')->post('/admin/specialties/createspecialty', 'App\Http\Controllers\Admin\SpecialtiesController@createspecialty')->name('createspecialty');
Route::middleware('auth')->post('/admin/specialties/deletespecialty', 'App\Http\Controllers\Admin\SpecialtiesController@deletespecialty')->name('deletespecialty');
Route::middleware('auth')->post('/admin/independents/addIndependent', 'App\Http\Controllers\Admin\IndependentsController@addIndependent')->name('addIndependent');
Route::middleware('auth')->post('/admin/independents/deleteAvailableTime', 'App\Http\Controllers\Admin\IndependentsController@deleteAvailableTime')->name('deleteAvailableTime');
Route::middleware('auth')->post('/admin/independents/deleteImg', 'App\Http\Controllers\Admin\IndependentsController@deleteImg')->name('deleteImg');
Route::middleware('auth')->post('/admin/independents/consultAvailableTime', 'App\Http\Controllers\Admin\IndependentsController@consultAvailableTime')->name('consultAvailableTime');
Route::middleware('auth')->get('/admin/independents/profile/{id}', 'App\Http\Controllers\Admin\IndependentsController@profile')->name('profile');
Route::middleware('auth')->get('/admin/independents/occupation/{id}', 'App\Http\Controllers\Admin\IndependentsController@occupation')->name('occupation');
Route::middleware('auth')->post('/admin/ratings/createrating', 'App\Http\Controllers\Admin\RatingsController@createrating')->name('createrating');
Route::group(['middleware' => ['auth']], function() {
    Route::resource('/admin/categories', 'App\Http\Controllers\Admin\CategoriesController');
    Route::resource('/admin/companies', 'App\Http\Controllers\Admin\CompaniesController');
    Route::resource('/admin/specialties', 'App\Http\Controllers\Admin\SpecialtiesController');
    Route::resource('/admin/independents', 'App\Http\Controllers\Admin\IndependentsController');
    Route::resource('/admin/ratings', 'App\Http\Controllers\Admin\RatingsController');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
