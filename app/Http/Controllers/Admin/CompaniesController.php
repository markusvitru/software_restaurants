<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Company;
use App\Http\Controllers\Controller;
use App\Specialty;
use App\Independent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompaniesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $specialties = Specialty::select('*')->where('type', 'empr')->orderBy('id', 'desc')->get();
        $companies = Company::select("*")->orderBy('id', 'desc')->get();
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        return view('admin.companies.index', compact('company', 'specialties', 'independent', 'user', 'companies'));
    }

    public function create()
    {
        $user = Auth::user();
        $specialties = Specialty::select('*')->where('type', 'empr')->orderBy('id', 'desc')->get();
        $categories = Category::select('*')->where('type', 'empr')->orderBy('id', 'desc')->get();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        return view('admin.companies.create', compact('specialties', 'categories', 'user', 'company','independent'));
    }

    public function store(Request $request)
    {
        $c = new Company();
        $c->user_id = $request->user_id;
        $c->name = $request->name;
        $c->phone = $request->phone;
        $c->city = $request->city;
        $c->email = $request->email;
        $c->address = $request->address;
        $c->web_site = $request->web_site;
        $c->facebook = $request->facebook;
        $c->instagram = $request->instagram;
        $c->contact = $request->contact;
        $c->specialties_id = $request->specialties_id;
        $c->categories_id = $request->categories_id;
        $c->save();
        //$company = Company::select('*')->orderBy('id', 'desc')->get();
        $specialties = Specialty::select('*')->where('type', 'empr')->orderBy('id', 'desc')->get();
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        return view('home', compact('company', 'specialties','independent','user'));
    }

    public function edit($id)
    {
        $comp = Company::findOrFail($id);
        $user = Auth::user();
        $specialties = Specialty::select('*')->where('type', 'empr')->orderBy('id', 'desc')->get();
        $categories = Category::select('*')->where('type', 'empr')->orderBy('id', 'desc')->get();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        return view('admin.companies.update', compact('company','comp','specialties', 'categories', 'user','independent'));
    }

    public function update(Request $request, $id)
    {
        $comp = Company::findOrFail($id);
        $comp->name = $request->name;
        $comp->phone = $request->phone;
        $comp->city = $request->city;
        $comp->email = $request->email;
        $comp->address = $request->address;
        $comp->web_site = $request->web_site;
        $comp->facebook = $request->facebook;
        $comp->instagram = $request->instagram;
        $comp->contact = $request->contact;
        $comp->specialties_id = $request->specialties_id;
        $comp->categories_id = $request->categories_id;
        if($request->file('logo') != ""){
            $logo = $request->name.'.' .$request->file('logo')->guessExtension();
            $request->file('logo')->move(
                base_path().'/public/images/logos_companies/', $logo
            );
            $comp->images = $logo;
        }
        $comp->save();
        $specialties = Specialty::select('*')->where('type', 'empr')->orderBy('id', 'desc')->get();
        $companies = Company::select("*")->orderBy('id', 'desc')->get();
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        return view('home', compact('company', 'specialties', 'independent', 'user', 'companies'));
    }
}
