<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Company;
use App\Http\Controllers\Controller;
use App\Independent;
use App\Specialty;
use App\CompanyIndependent;
use App\AvailableTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndependentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $specialties = Specialty::select('*')->where('type', 'empl')->orderBy('id', 'desc')->get();
        $independents = Independent::select('*')->orderBy('id', 'desc')->get();
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        return view('admin.independents.index', compact('independents', 'company', 'specialties', 'independent', 'user'));
    }

    public function create()
    {
        $user = Auth::user();
        $specialties = Specialty::select('*')->where('type', 'empl')->orderBy('id', 'desc')->get();
        $categories = Category::select('*')->where('type', 'empl')->orderBy('id', 'desc')->get();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        return view('admin.independents.create', compact('specialties', 'categories', 'user', 'company', 'independent'));
    }

    public function store(Request $request)
    {
        $independent = new Independent();
        $independent->user_id = $request->user_id;
        $independent->name = $request->name;
        $independent->phone = $request->phone;
        $independent->city = $request->city;
        $independent->email = $request->email;
        $independent->identification = $request->identification;
        $independent->specialties_id = $request->specialties_id;
        $independent->categories_id = $request->categories_id;
        $independent->images = $request->images;
        $independent->save();
        $independents = Independent::select('*')->orderBy('id', 'desc')->get();
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        return view('home', compact('independents','company', 'independent', 'user'));
    }

    public function edit($id){
        $indep = Independent::findOrFail($id);
        $user = Auth::user();
        $specialties = Specialty::select('*')->where('type', 'empl')->orderBy('id', 'desc')->get();
        $categories = Category::select('*')->where('type', 'empl')->orderBy('id', 'desc')->get();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        $availableTime = AvailableTime::select('*')->where('independent_id',$id)->get();
        return view('admin.independents.update', compact('company','indep','specialties', 'categories', 'user','independent','availableTime'));
    }
    public function consultAvailableTime(Request $request){
        if ($request->get('id')) {
            $id = $request->get('id');
            $availableTime = AvailableTime::select('*')->where('independent_id',$id)->get();
            return response()->json($availableTime);
        }
    }
    public function addIndependent(Request $request)
    {
        if ($request->get('id')) {
            $id = $request->get('id');
            $user = Auth::user();
            $company = Company::select('id')->where('user_id', $user->id)->first();
            $id_company = $company['id'];
            $companyIndependent = new CompanyIndependent();
            $companyIndependent->independent_id = $id;
            $companyIndependent->company_id = $id_company;
            $companyIndependent->available_time_id = $request->get('ids');
            $companyIndependent->save();
        }
    }
    public function profile($id){
        $specialties = Specialty::select('*')->where('type', 'empl')->orderBy('id', 'desc')->get();
        $independentinfo = Independent::select('*')->where('id', $id)->first();
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        return view('admin.independents.profile', compact('independentinfo', 'company', 'specialties', 'independent', 'user'));
    }
    public function occupation($id){
        $independentinfo = Independent::select('*')->where('id', $id)->first();
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        $availableTime = AvailableTime::select('*')->where('independent_id',$id)->get();
        return view('admin.independents.occupation', compact('company','independentinfo', 'user','independent','availableTime'));
    }
    public function update(Request $request, $id)
    {
        $indep = Independent::findOrFail($id);
        $indep->user_id = $request->user_id;
        $indep->name = $request->name;
        $indep->phone = $request->phone;
        $indep->city = $request->city;
        $indep->email = $request->email;
        $indep->identification = $request->identification;
        $indep->specialties_id = $request->specialties_id;
        $indep->categories_id = $request->categories_id;
        $imgs = '';
        // File upload configuration
        $targetDir = base_path() . "/public/images/jobs_independents/";
        $allowTypes = array('jpg', 'png', 'jpeg', 'gif');
        $fileNames = array_filter($_FILES['files']['name']);
        if (!empty($fileNames)) {
            foreach ($_FILES['files']['name'] as $key => $val) {
                // File upload path
                $fileName = basename($_FILES['files']['name'][$key]);
                $targetFilePath = $targetDir . $fileName;
                if ($key == 0) {
                    $imgs = $fileName;
                } else {
                    $imgs = $imgs . ' / ' . $fileName;
                }
                // Check whether file type is valid
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
                if (in_array($fileType, $allowTypes)) {
                    // Upload file to server
                    move_uploaded_file($_FILES["files"]["tmp_name"][$key], $targetFilePath);
                } else {
                    echo 'Error al guardar imgs trabajos ';
                }
            }
        }
        if ($imgs != '') {
            $indep->images = $imgs . ' / ' . $request->images;
        }
        $indep->save();

        $specialties = Specialty::select('*')->where('type', 'empl')->orderBy('id', 'desc')->get();
        $independents = Independent::select('*')->orderBy('id', 'desc')->get();
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        $conta = 0;
        if ($request->days1 != '') {
            $conta=1;
        }
        if ($request->days2 != '') {
            $conta = 2;
        }
        if ($request->days3 != '') {
            $conta = 3;
        }
        if ($request->days4 != '') {
            $conta = 4;
        }
        if ($request->days5 != '') {
            $conta = 5;
        }
        if ($request->days6 != '') {
            $conta = 6;
        }
        if ($request->days7 != '') {
            $conta = 7;
        }
        if ($request->days8 != '') {
            $conta = 8;
        }
        if ($request->days9 != '') {
            $conta = 9;
        }
        if ($request->days10 != '') {
            $conta = 10;
        }
        if ($request->days11 != '') {
            $conta = 11;
        }
        if ($request->days12 != '') {
            $conta = 12;
        }
        $data = $request->all();
        $i = 1;
        $ind = $id;
        for ($i = 1; $i <= $conta; $i++) {
            $availabletime = $data;
            $d = 'days' . $i;
            $st = 'start_time' . $i;
            $et = 'end_time' . $i;
            $availabletime['days'] = $request->$d;
            $availabletime['start_time'] = $request->$st;
            $availabletime['end_time'] = $request->$et;
            $availabletime['independent_id'] = $ind;
            $avail = AvailableTime::create($availabletime);
        }
        return view('home', compact('independents', 'company', 'specialties', 'independent', 'user'));
    }
    public function deleteAvailableTime(Request $request)
    {
        if ($request->get('id')) {
            $id = $request->get('id');
            $availabletime = AvailableTime::findOrFail($id);
            $availabletime->delete();
        }
    }
    public function deleteImg(Request $request)
    {
        if ($request->get('delimg')) {
            $delimg = $request->get('delimg');
            $img = $delimg;
            $ruta = base_path() . '/public/images/jobs_independents/' . $img;
            unlink($ruta);
            $id = $request->get('ida');
            $uimgs = $request->get('uimgs');
            $independent = Independent::findOrFail($id);
            $independent->url_imgs = $uimgs;
            if ($independent->save()) {
                $resp = 'Foto eliminado correctamente';
            } else {
                $resp = 'La foto no pudo ser eliminado';
            }
        }
    }
}
