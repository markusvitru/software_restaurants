<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Http\Controllers\Controller;
use App\Specialty;
use App\Independent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SpecialtiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $specialties = Specialty::select('*')->orderBy('id', 'desc')->get();
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        return view('admin.specialties.index', compact('specialties', 'company', 'independent', 'user'));
    }

    public function createspecialty()
    {
        $n = $_POST['name'];
        $type = $_POST['type'];
        $specialty = new Specialty();
        $specialty->name = $n;
        $specialty->type = $type;
        $specialty->save();
    }

    public function edit($id)
    {
        $specialty = Specialty::findOrFail($id);

        return view('admin.specialties.update', compact('specialty'));
    }

    public function update(Request $request, $id)
    {
        $specialty = Specialty::findOrFail($id);
        $specialty->name = $request->name;
        $specialty->type = $request->typeUpdate;
        $specialty->save();

        return redirect()->back();
    }

    public function deletespecialty(Request $request)
    {
        if ($request->get('id')) {
            $id = $request->get('id');
            $categoria = Specialty::findOrFail($id);
            $categoria->delete();
        }
    }
}
