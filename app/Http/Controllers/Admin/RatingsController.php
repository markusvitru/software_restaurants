<?php

namespace App\Http\Controllers\Admin;

use App\Rating;
use App\Company;
use App\Specialty;
use App\Independent;
use App\CompanyIndependent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RatingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        $companies = Company::select('id')->where('user_id', $user->id)->first();
        $id_company = $companies['id'];
        //$ratings = Rating::select('score','independent_id')->where('company_id',$id_company)->get();
        $companyIndependent = CompanyIndependent::select('*')->where('company_id',$id_company)->orderBy('id', 'desc')->get();
        $specialties = Specialty::select('*')->where('type', 'empl')->orderBy('id', 'desc')->get();
        $independents = Independent::select('*')->orderBy('id', 'desc')->get();
        return view('admin.ratings.index', compact('companyIndependent', 'company', 'independent', 'user', 'specialties', 'independents'));
    }

    public function createrating()
    {
        $id = $_POST['id'];
        $rating_ = $_POST['rating'];
        $rating = new Rating();
        $rating->independent_id = $id;
        $rating->score = $rating_;
        $user = Auth::user();
        $companies = Company::select('id')->where('user_id', $user->id)->first();
        $rating->company_id = $companies['id'];
        $rating->comment = $_POST['comment'];
        $rating->save();
    }

}
