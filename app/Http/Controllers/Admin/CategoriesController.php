<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Company;
use App\Independent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $category = Category::select('*')->orderBy('id', 'desc')->get();
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        $independent = Independent::select('*')->where('email', $user->email)->get();
        return view('admin.categories.index', compact('category', 'company', 'independent', 'user'));
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function edit($id)
    {
        $users = Auth::user();
        $category = Category::findOrFail($id);
        $company = Company::select('*')->where('email', $users->email)->get();
        return view('admin.categories.update', compact('category', 'company'));
    }

    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->type = $request->typeUpdate;
        $category->save();

        return redirect()->back();
    }

    public function createcategoria()
    {
        $n = $_POST['name'];
        $type = $_POST['type'];
        $categoria = new Category();
        $categoria->name = $n;
        $categoria->type = $type;
        $categoria->save();
    }

    public function deleteCategorie(Request $request)
    {
        if ($request->get('id')) {
            $id = $request->get('id');
            $categoria = Category::findOrFail($id);
            $categoria->delete();
        }
    }
}
