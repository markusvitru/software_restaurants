<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Company;
use App\Independent;
use App\Specialty;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        if($data['typeform'] == 'empr'){
            $c = new Company();
            $c->user_id = 99999;
            $c->name = $data['name'];
            $c->phone = $data['phone_company'];
            $c->city = $data['city_company'];
            $c->email = $data['email'];
            $c->address = $data['address'];
            $c->web_site = $data['web_site'];
            $c->facebook = $data['facebook'];
            $c->instagram = $data['instagram'];
            $c->contact = $data['contact'];
            $c->specialties_id = $data['specialties_id_company'];
            $c->categories_id = $data['categories_id_company'];
            $c->save();
        }elseif($data['typeform'] == 'empl'){
            $independent = new Independent();
            $independent->user_id = 99999;
            $independent->name = $data['name'];
            $independent->phone = $data['phone'];
            $independent->city = $data['city'];
            $independent->email = $data['email'];
            $independent->identification = $data['identification'];
            $independent->specialties_id = $data['specialties_id'];
            $independent->categories_id = $data['categories_id'];
            $independent->images = '';
            $independent->save();
        }
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    public function getType(Request $request){
        $type = $request->get('type');
        if($type == 'empr'){
            $specialtiesempr = Specialty::select('*')->where('type', 'empr')->orderBy('id', 'desc')->get();
            $categoriesempr = Category::select('*')->where('type', 'empr')->orderBy('id', 'desc')->get();
            return response()->json([
                'specialtiesempr' => $specialtiesempr,
                'categoriesempr'  => $categoriesempr
            ]);
        }else if($type == 'empl'){
            $specialties = Specialty::select('*')->where('type', 'empl')->orderBy('id', 'desc')->get();
            $categories = Category::select('*')->where('type', 'empl')->orderBy('id', 'desc')->get();
            //return $categories;
            return response()->json([
                'specialties' => $specialties,
                'categories'  => $categories
            ]);
        }

    }
}
