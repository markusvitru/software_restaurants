<?php

namespace App\Http\Controllers;

use App\Company;
use App\Independent;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $company = Company::select('*')->where('email', $user->email)->get();
        foreach($company as $row){
            if($row['user_id'] == 99999){
                $comp = Company::findOrFail($row['id']);
                $comp->user_id = $user->id;
                $comp->save();
            }
        }
        $independent = Independent::select('*')->where('email', $user->email)->get();
        foreach($independent as $row){
            if($row['user_id'] == 99999){
                $ind = Independent::findOrFail($row['id']);
                $ind->user_id = $user->id;
                $ind->save();
            }
        }
        return view('home', compact('company', 'independent', 'user'));
    }
}
