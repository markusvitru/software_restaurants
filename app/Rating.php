<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

    protected $guard = 'admin';

    protected $fillable = [
        'score',
        'independent_id',
        'company_id',
        'comment'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    protected $dates  = [
        'created_at',
        'updated_at'
    ];
}
