<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvailableTime extends Model
{

    protected $guard = 'admin';

    protected $fillable = [
        'days',
        'start_time',
        'end_time',
        'independent_id',
        'company_id',
        'is_active'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    protected $dates  = [
        'created_at',
        'updated_at'
    ];
}
