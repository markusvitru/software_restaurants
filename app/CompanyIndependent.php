<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyIndependent extends Model
{

    protected $guard = 'admin';

    protected $fillable = [
        'independent_id',
        'company_id',
        'available_time_id',
        'is_active'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    protected $dates  = [
        'created_at',
        'updated_at'
    ];
}
