<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Specialty;

class Independent extends Model
{
    protected $guard = 'admin';

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'city',
        'phone',
        'email',
        'identification',
        'specialties_id',
        'images',
        'categories_id',
        'is_active'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    protected $dates  = [
        'created_at',
        'updated_at'
    ];
    public function specialties()
    {
        return $this->belongsTo(Specialty::class)
            ->select(['id', 'name']);
    }
}
